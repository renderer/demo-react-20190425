import React, { useState, useCallback } from "react";

import store from "./store";

import { connect, Provider } from "react-redux";
import { changeUsername, loadMore } from "./store/repo.slice";

const UsernameInput = ({ onChangeUser }) => {
  const [username, setUsername] = useState("");

  const handleKeyDown = useCallback(
    e => {
      if (e.which === 13) {
        onChangeUser && onChangeUser(username);
      }
    },
    [username, onChangeUser]
  );

  return (
    <input
      value={username}
      onChange={e => setUsername(e.target.value)}
      onKeyDown={handleKeyDown}
    />
  );
};

const UsernameInputContainer = connect(
  null,
  dispatch => ({
    onChangeUser: username => dispatch(changeUsername(username))
  })
)(UsernameInput);

const RepoList = ({ username, repos, loading, total, onLoadMore }) => {
  return (
    <div>
      {username && (
        <div>
          Loaded: {repos.length} / {total}
        </div>
      )}
      <ul>
        {repos.map(repo => (
          <li key={repo.id}>{repo.name}</li>
        ))}
      </ul>
      {loading && <div>Loading repos...</div>}
      {!loading && repos.length < total && (
        <button onClick={() => onLoadMore && onLoadMore()}>Load more</button>
      )}
    </div>
  );
};

const RepoListContainer = connect(
  state => ({
    username: state.repo.username,
    total: state.repo.total,
    repos: state.repo.repos,
    loading: state.repo.loading
  }),
  dispatch => ({
    onLoadMore: () => dispatch(loadMore())
  })
)(RepoList);

const App = () => {
  return (
    <div>
      <UsernameInputContainer />
      <RepoListContainer />
    </div>
  );
};

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
);
