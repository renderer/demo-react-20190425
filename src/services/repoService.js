import http from '../utils/http';

const repoService = {};

repoService.getRepos = (username, page) =>
  http
    .get(`/users/${username}/repos`, { params: { page } })
    .then(res => res.data);

repoService.getUserInfo = username =>
  http.get(`/users/${username}`).then(res => res.data);

export default repoService;
