import { createSlice } from "redux-starter-kit";

import repoService from "../services/repoService";

const repoSlice = createSlice({
  slice: "repo", // slice is optional, and could be blank ''
  initialState: {
    username: "",
    page: 1,
    repos: [],
    total: 0,
    error: undefined,
    loading: false
  },
  reducers: {
    changeUsername: (state, action) => ({
      ...state,
      username: action.payload,
      page: 1,
      repos: []
    }),
    updateTotal: (state, action) => ({
      ...state,
      total: action.payload
    }),
    loadRepoStart: (state, action) => ({
      ...state,
      loading: true
    }),
    loadRepoSuccess: (state, action) => ({
      ...state,
      loading: false,
      repos: [...state.repos, ...action.payload.repos],
      page: action.payload.page
    }),
    loadRepoFailed: (state, action) => ({
      ...state,
      loading: false,
      error: action.payload
    })
  }
});

const loadRepos = (username, page = 1) => {
  return async dispatch => {
    try {
      dispatch(repoSlice.actions.loadRepoStart());
      const repos = await repoService.getRepos(username, page);
      dispatch(
        repoSlice.actions.loadRepoSuccess({
          repos,
          page
        })
      );
    } catch (e) {
      dispatch(repoSlice.actions.loadRepoFailed(e.message));
    }
  };
};

export const changeUsername = username => {
  return async dispatch => {
    dispatch(repoSlice.actions.changeUsername(username));
    const userInfo = await repoService.getUserInfo(username);
    dispatch(repoSlice.actions.updateTotal(userInfo.public_repos));
    await dispatch(loadRepos(username));
  };
};

export const loadMore = () => {
  return async (dispatch, getState) => {
    const state = getState();
    const { username, page, loading } = state.repo;
    if (loading) {
      return;
    }
    await dispatch(loadRepos(username, page + 1));
  };
};

export default repoSlice;
