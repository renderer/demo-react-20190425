import { configureStore } from "redux-starter-kit";

import { combineReducers } from "redux";

import repoSlice, { changeUsername, loadMore } from "./repo.slice";

const rootReducer = combineReducers({
  repo: repoSlice.reducer
});

const store = configureStore({ reducer: rootReducer });

store.subscribe(() => {
  const state = store.getState();
  console.log(state);
});

export default store;
