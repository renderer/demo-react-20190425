import React, { useState, useEffect } from 'react';

// const List = ({ text, data }) => {
//   return (
//     <React.Fragment>
//       <ul>
//         {data.map((i, index) => <li key={index}>{i}</li>)}
//       </ul>
//     </React.Fragment>
//   );
// };

// const Dummy = () => {
//   console.log('rendering dummy component!');
//   return 'dcm';
// };

// const App = () => {
//   const [data, setData] = useState({
//     list: [],
//     name: 'dcm'
//   });

//   const addNew = () => {
//     setData({
//       ...data,
//       list: [...data.list, Math.floor(Math.random() * 10)]
//     });
//   };

//   return (
//     <div className="App">
//       {data.name}
//       <button onClick={() => addNew()}>Add new!</button>
//       <List text="vcl" data={data.list} />
//     </div>
//   );
// };

// // class App extends React.Component {
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //       data: [],
// //       name: 'dcm'
// //     };
// //   }
// //   addNew() {
// //     this.setState({
// //       data: [...this.state.data, Math.floor(Math.random() * 10)]
// //     });
// //   }
// //   render() {
// //     const { data, name } = this.state;

// //     return (
// //       <div className="App">
// //         {name}
// //         <button onClick={() => this.addNew()}>Add new!</button>
// //         <List text="vcl" data={data} />
// //       </div>
// //     );
// //   }
// // }

// const AppWrapper = () => {
//   console.log('rendering AppWrapper!');
//   return <App />;
// };

import repoService from './services/repoService';

import store from './store';

import { connect, Provider } from 'react-redux';

const RepoList = ({ username, onItemClick }) => {
  const [data, setData] = useState({
    page: 1,
    repos: [],
    totalRepos: 0
  });

  const loadRepos = async (username, page = 1) => {
    const loadedRepos = data.repos;
    if (page === 1) loadedRepos.length = 0;
    const repos = await repoService.getRepos(username, page);
    const userInfo = await repoService.getUserInfo(username);
    setData({
      ...data,
      repos: [...loadedRepos, ...repos],
      page,
      totalRepos: userInfo.public_repos
    });
  };

  const loadMore = () => {
    loadRepos(username, data.page + 1);
  };

  useEffect(
    () => {
      if (!username) return;
      loadRepos(username);
    },
    [username]
  );

  const { repos, page, totalRepos } = data;

  return (
    <ul>
      {repos.map(repo => (
        <li
          onClick={() => onItemClick && onItemClick(repo.full_name)}
          key={repo.id}>
          {repo.name}
        </li>
      ))}
      {username &&
        page * 30 < totalRepos &&
        <button onClick={loadMore}>Load more</button>}
      {username && <span>Total repos: {repos.length} / {totalRepos}</span>}
    </ul>
  );
};

const App = () => {
  const [username, setUsername] = useState('');
  const [currentUser, setCurrentUser] = useState(false);

  const handleKeydown = e => {
    if (e.which === 13) {
      setCurrentUser(username);
    }
  };

  return (
    <div>
      <input
        value={username}
        onChange={e => setUsername(e.target.value)}
        onKeyDown={handleKeydown}
      />
      <RepoList username={currentUser} onItemClick={item => alert(item)} />
    </div>
  );
};

export default () => <Provider store={store}><App /></Provider>;
